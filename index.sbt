// Function#1: Write a function that prints the string “Hello,world.”
//============================= Function 1 =============================
println("Function 1")
println("----------")

def Function1() {
  println("Hello, world!")
}
Function1()

// Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
// The function should ﬁnd the index of substring in string and print that index.
//============================= Function 2 =============================
println("\nFunction 2")
println("----------")

def Function2(str: String, substr: String) {
  if (str.contains(substr)) {
    println("Finding ''" + substr + "'' in ''" + str + "''")
    println(str + " contains " + substr)
    val subIndex = str.indexOf(substr)
    println(substr + " is found at index " + subIndex)
  } else {
    println("Finding ''" + substr + "'' in ''" + str + "''")
    println(str + " does not contains " + substr)
  }
}
Function2("SearchMe", "Me")
println()
Function2("SearchMe", "They")

// Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
// the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
// ODD index. The array passed into this function must contain at least 100 randomly generated values. 
// Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
//============================= Function 3 =============================
println("\nFunction 3")
println("----------")

val r = scala.util.Random
var arr = new Array[Int](100)
for (i <- 0 to (arr.length - 1))
  arr(i) = r.nextInt(300)

def Function3(args: Array[Int]) {
  println("Sorted Order:")
  val arrSorted = arr.sorted
  for (i <- 0 to (arrSorted.length - 1)) {
    if (i != arrSorted.length - 1)
      print(arrSorted(i) + ", ")
    else
      print(arrSorted(i) + "\n")
  }
  println("\nReverse Order:")
  val arrReverse = arrSorted.reverse
  for (i <- 0 to (arrReverse.length - 1)) {
    if (i != arrReverse.length - 1)
      print(arrReverse(i) + ", ")
    else
      print(arrReverse(i) + "\n")
  }
  println("\nOnly Odd Index:")
  val odds = arrSorted.indices.collect { case i if i % 2 == 1 => arrSorted(i) }
  for (i <- 0 to (odds.length - 1)) {
    if (i != odds.length - 1)
      print(odds(i) + ", ")
    else
      print(odds(i) + "\n")
  }
}
Function3(arr)

// Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
//============================= Function 4 =============================
println("\nFunction 4")
println("----------")

val r2 = scala.util.Random
var arr2 = new Array[Int](100)
for (i <- 0 to (arr2.length - 1))
  arr2(i) = r2.nextInt(300)
println("Unsorted:")
for (i <- 0 to (arr2.length - 1)) {
  if (i != arr2.length - 1)
    print(arr2(i) + ", ")
  else
    print(arr2(i) + "\n")
}
def Function4(arr2: Array[Int]) = {
  for (i <- 0 until arr2.length - 1) {
    var index = i
    for (j <- i + 1 until arr2.length) {
      if (arr2(j) < arr2(index)) index = j
    }
    if (index != i) {
      val temp = arr2(i)
      arr2(i) = arr2(index)
      arr2(index) = temp
    }
  }
  println("\nSelection Sort:")
  for (i <- 0 to (arr2.length - 1)) {
    if (i != arr2.length - 1)
      print(arr2(i) + ", ")
    else
      print(arr2(i) + "\n")
  }
}
Function4(arr2)

// Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
// The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
//============================= Function 5 =============================
println("\nFunction 5")
println("----------")

def Function5(n: Int): Int = {
  var a = 0
  var b = 1
  var i = 0

  while (i < n) {
    val c = a + b
    a = b
    b = c
    i = i + 1
  }
  return a
}
for (i <- 1 to 20)
  println("Fib(" + i + ") = " + Function5(i))

// Function#6: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. Compare the
// computation time and lines of code with Function#5.
//============================= Function 6 =============================
println("\nFunction 6")
println("----------")

def Function6(num: Int): Int = num match {
  case 0 => 0
  case 1 => 1
  case n => Function6(n - 1) + Function6(n - 2)
}
for (i <- 1 to 20)
  println("Fib(" + i + ") = " + Function6(i))
 
 
//============================= Computation Time Function =============================
def time[R](block: => R): R = {
  val t0 = System.nanoTime()
  val result = block
  val t1 = System.nanoTime()
  println("Elasped time: " + (t1 - t0) + "ns")
  result
}

println("\nNano seconds of computing Fib(20) by Iterative Fib Function: ")
println(time(Function5(20)))

println("\nNano seconds of computing Fib(20) by Recursive Fib Function: ")
println(time(Function6(20)))

// Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
// return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
// 0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
//============================= Function 7 =============================
println("\nFunction 7")
println("----------")

val mA = new Array[Array[Int]](5)
val mB = new Array[Array[Int]](5)
val mC = new Array[Array[Int]](5)
val r3 = scala.util.Random
def RandomMatrix(mA: Array[Array[Int]], mB: Array[Array[Int]], mC: Array[Array[Int]]) {
  for (i <- 0 to (mA.length - 1)) {
    val temp = new Array[Int](5)
    for (i <- 0 to (temp.length - 1))
      temp(i) = r3.nextInt(300)
    mA(i) = temp
  }
  for (i <- 0 to (mB.length - 1)) {
    val temp = new Array[Int](5)
    for (i <- 0 to (temp.length - 1))
      temp(i) = r3.nextInt(300)
    mB(i) = temp
  }
  for (i <- 0 to (mC.length - 1)) {
    val temp = new Array[Int](5)
    for (i <- 0 to (temp.length - 1))
      temp(i) = 0
    mC(i) = temp
  }
}
RandomMatrix(mA, mB, mC)

def PrintMatrix(m: Array[Array[Int]]) {
  for (i <- 0 to (m.length - 1)) {
    for (j <- 0 to (m.length - 1)) {
      print(m(i)(j) + " ")
    }
    println
  }
}

def Function7(mA: Array[Array[Int]], mB: Array[Array[Int]], mC: Array[Array[Int]]) {
  for (i <- 0 to (mA.length - 1)) {
    val temp = new Array[Int](5)
    for (j <- 0 to (mB.length - 1)) {
      var sum = 0
      for (k <- 0 to (mB.length - 1)) {
        sum = sum + mA(i)(k) * mB(k)(j)
      }
      temp(j) = sum
    }
    mC(i) = temp
  }
  println("Matrix A: ")
  PrintMatrix(mA)
  println("\nMatrix B: ")
  PrintMatrix(mB)
  println("\nMatrix C = Matrix A * Matrix B: ")
  PrintMatrix(mC)
}
Function7(mA, mB, mC)

// Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
// equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
// by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
// each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
// and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function. 
// The output ﬁle should insert the string _Output into the original ﬁlename. If the input ﬁle is “HammingData.csv” then the output ﬁle would be HammingData_Output.csv.
//============================= Function 8 =============================
println("\nFunction 8")
println("----------")

val fIn = "index.txt"
def Function8(fIn: String) {
  import scala.io.Source
  import java.io.File
  import java.io.PrintWriter
  var lines = new Array[String](4)
  var strList = new Array[String](8)
  var i = 0
  var k = 0
  val writer = new PrintWriter(new File("index_Output.txt"))
  for (line <- Source.fromFile(fIn).getLines()) {
    lines(i) = line
    i = i + 1
  }
  for (i <- 0 to (lines.length - 1)) {

    val blank = lines(i).indexOf(" ")
    strList(k) = lines(i).slice(0, blank)
    strList(k + 1) = lines(i).slice(blank + 1, lines(i).length)
    val ham = strList(k).zip(strList(k + 1)).count(pair => pair._1 != pair._2)
    print(strList(k) + ":" + strList(k + 1) + ":" + ham + "\n")
    writer.write(strList(k) + ":" + strList(k + 1) + ":" + ham + "   ")
    k = k + 2
  }
  writer.close()
}
Function8(fIn)

// Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
// store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
//============================= Function 9 =============================
println("\nFunction 9")
println("----------")

class csvReader() {
  import scala.io.Source
  def Function9(fIn2: String) {
    var lines2 = new Array[String](6)
    var csvRow = new Array[String](24)
    var n = 0
    var k = 0

    for (line <- Source.fromFile(fIn2).getLines()) {
      lines2(n) = line
      n = n + 1
    }
    for (i <- 0 to lines2.length - 1) {
      var comma = new Array[Int](30)
      comma(k) = -1
      for (j <- 0 to lines2(i).length - 1) {
        if (lines2(i)(j) == ',') {
          comma(k + 1) = j
          csvRow(k) = lines2(i).slice(comma(k) + 1, j)
          k = k + 1
        }
        if (j == lines2(i).length - 1) {
          comma(k + 1) = j
          csvRow(k) = lines2(i).slice(comma(k) + 1, j + 1)
          k = k + 1
        }
      }
    }
    for (i <- 0 to (lines2.length - 1)) {
      if (i == 0) print("Header: ") else print("\nRow " + i + ":  ")
      for (k <- (i * 4) to (i * 4 + 3))
        if (i == 0) print(csvRow(k) + "      ") else print(csvRow(k) + "   ")
    }
  }
}
val ReadingCSV = new csvReader()
ReadingCSV.Function9("index.csv")

// Task #10: Implement the equivalent of the Class diagram shown in the image link location below:
// https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
// This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.
//============================= Function 10 =============================
println("\n\nFunction 10")
println("----------")

class Shape(color: String, filled: Boolean) {
  def getColor(): String = { return color }
  def isFilled(): Boolean = { return filled }
  def description(): String = { return "I am a Shape" }
}

class Circle(color: String, filled: Boolean, radius: Double) extends Shape(color, filled) {
  def getRadius(): Double = { return radius }
  def getArea(): Double = { return (3.1416 * radius * radius) }
  def getPerimeter(): Double = { return (3.1416 * 2 * radius) }
  override def description(): String = { return "I am a Circle and a Shape" }
}

class Rectangle(color: String, filled: Boolean, width: Double, length: Double) extends Shape(color, filled) {
  def getWidth(): Double = { return width }
  def getLength(): Double = { return length }
  def getArea(): Double = { return (length * width) }
  def getPerimeter(): Double = { return (2 * (length + width)) }
  override def description(): String = { return "I am a Rectangle and a Shape" }
}

class Square(color: String, filled: Boolean, width: Double, length: Double, side: Double) extends Rectangle(color, filled, width, length) {
  def getSide(): Double = { return side }
  override def getArea(): Double = { return (side * side) }
  override def getPerimeter(): Double = { return (4 * side) }
  override def description(): String = { return "I am a Square, a Rectangle and a Shape" }
}

val shape = new Shape("red", true)
println(shape.description())
var color = shape.getColor()
var filled = shape.isFilled()

val circle = new Circle(color, filled, 1.0)
println(circle.description())

val rectangle = new Rectangle(color, filled, 1.0, 1.0)
println(rectangle.description())

val square = new Square(color, filled, 1.0, 1.0, 2.0)
println(square.description())

println("\nThe radius of a Circle is " + circle.getRadius())
println("=> The area of this Circle is " + circle.getArea())
println("=> The perimeter of this Circle is " + circle.getPerimeter())

println("\nThe length of a Rectangle is " + rectangle.getLength() + ", and the width of a Rectangle is " + rectangle.getWidth())
println("=> The area of this Rectangle is " + rectangle.getArea())
println("=> The perimeter of this Rectangle is " + rectangle.getPerimeter())

println("\nThe side of a Square is " + square.getSide())
println("=> The area of this Square is " + square.getArea())
println("=> The area of this Square is " + square.getPerimeter())
