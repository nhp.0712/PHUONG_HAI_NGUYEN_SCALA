1. Function#1: Write a function that prints the string “Hello,world.”
2. Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
    The function should ﬁnd the index of substring in string and print that index.
3. Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
    the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
    ODD index. The array passed into this function must contain at least 100 randomly generated values. 
    Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
4. Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
5. Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
    The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
6. Function#6: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. Compare the
    computation time and lines of code with Function#5.
7. Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
    return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
    0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
8. Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
    equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
    by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
    each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
    and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function. 
    The output ﬁle should insert the string _Output into the original ﬁlename. If the input ﬁle is “HammingData.csv” then the output ﬁle would be HammingData_Output.csv.
9. Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
    store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
10. Task #10: Implement the equivalent of the Class diagram shown in the image link location below:
    https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
    This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.



Instructions for compiling and running this Scala file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_SCALA
. Compile and run by command: scala index.sbt
-> the solutions for all functions will show up as the example below

2. Using Command Prompt for Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Compile and run by command: scala index.sbt
-> the solutions for all functions will show up as the example below



                            Example Solutions for all functions
Function 1
----------
Hello, world!

Function 2
----------
Finding ''Me'' in ''SearchMe''
SearchMe contains Me
Me is found at index 6

Finding ''They'' in ''SearchMe''
SearchMe does not contains They

Function 3
----------
Sorted Order:
0, 3, 3, 5, 6, 11, 12, 14, 20, 20, 24, 27, 32, 33, 33, 38, 42, 42, 44, 52, 58, 58, 58, 59, 59, 63, 63, 68, 68, 80, 80, 81, 84, 85, 86, 89, 94, 99, 107, 107, 108, 108, 111, 120, 121, 123, 128, 130, 135, 136, 138, 146, 166, 167, 168, 181, 183, 188, 196, 198, 203, 206, 207, 210, 210, 210, 211, 214, 215, 215, 215, 216, 225, 225, 226, 226, 227, 227, 228, 233, 234, 251, 254, 255, 261, 263, 272, 272, 276, 281, 282, 282, 284, 288, 289, 291, 294, 295, 296, 296

Reverse Order:
296, 296, 295, 294, 291, 289, 288, 284, 282, 282, 281, 276, 272, 272, 263, 261, 255, 254, 251, 234, 233, 228, 227, 227, 226, 226, 225, 225, 216, 215, 215, 215, 214, 211, 210, 210, 210, 207, 206, 203, 198, 196, 188, 183, 181, 168, 167, 166, 146, 138, 136, 135, 130, 128, 123, 121, 120, 111, 108, 108, 107, 107, 99, 94, 89, 86, 85, 84, 81, 80, 80, 68, 68, 63, 63, 59, 59, 58, 58, 58, 52, 44, 42, 42, 38, 33, 33, 32, 27, 24, 20, 20, 14, 12, 11, 6, 5, 3, 3, 0

Only Odd Index:
3, 5, 11, 14, 20, 27, 33, 38, 42, 52, 58, 59, 63, 68, 80, 81, 85, 89, 99, 107, 108, 120, 123, 130, 136, 146, 167, 181, 188, 198, 206, 210, 210, 214, 215, 216, 225, 226, 227, 233, 251, 255, 263, 272, 281, 282, 288, 291, 295, 296

Function 4
----------
Unsorted:
78, 39, 99, 203, 106, 164, 169, 210, 263, 13, 258, 249, 111, 217, 144, 119, 215, 4, 191, 239, 250, 14, 207, 138, 41, 142, 139, 234, 222, 247, 36, 1, 37, 11, 208, 124, 176, 105, 197, 291, 238, 129, 278, 0, 186, 56, 212, 56, 106, 240, 67, 58, 32, 143, 139, 202, 26, 276, 291, 104, 262, 236, 276, 223, 77, 200, 127, 45, 176, 103, 75, 227, 201, 278, 61, 191, 188, 259, 258, 177, 10, 96, 90, 246, 287, 150, 167, 30, 216, 266, 170, 265, 240, 99, 258, 179, 192, 279, 103, 14

Selection Sort:
0, 1, 4, 10, 11, 13, 14, 14, 26, 30, 32, 36, 37, 39, 41, 45, 56, 56, 58, 61, 67, 75, 77, 78, 90, 96, 99, 99, 103, 103, 104, 105, 106, 106, 111, 119, 124, 127, 129, 138, 139, 139, 142, 143, 144, 150, 164, 167, 169, 170, 176, 176, 177, 179, 186, 188, 191, 191, 192, 197, 200, 201, 202, 203, 207, 208, 210, 212, 215, 216, 217, 222, 223, 227, 234, 236, 238, 239, 240, 240, 246, 247, 249, 250, 258, 258, 258, 259, 262, 263, 265, 266, 276, 276, 278, 278, 279, 287, 291, 291

Function 5
----------
Fib(1) = 1
Fib(2) = 1
Fib(3) = 2
Fib(4) = 3
Fib(5) = 5
Fib(6) = 8
Fib(7) = 13
Fib(8) = 21
Fib(9) = 34
Fib(10) = 55
Fib(11) = 89
Fib(12) = 144
Fib(13) = 233
Fib(14) = 377
Fib(15) = 610
Fib(16) = 987
Fib(17) = 1597
Fib(18) = 2584
Fib(19) = 4181
Fib(20) = 6765

Function 6
----------
Fib(1) = 1
Fib(2) = 1
Fib(3) = 2
Fib(4) = 3
Fib(5) = 5
Fib(6) = 8
Fib(7) = 13
Fib(8) = 21
Fib(9) = 34
Fib(10) = 55
Fib(11) = 89
Fib(12) = 144
Fib(13) = 233
Fib(14) = 377
Fib(15) = 610
Fib(16) = 987
Fib(17) = 1597
Fib(18) = 2584
Fib(19) = 4181
Fib(20) = 6765

Nano seconds of computing Fib(20) by Iterative Fib Function: 
Elasped time: 26684ns
6765

Nano seconds of computing Fib(20) by Recursive Fib Function: 
Elasped time: 66095ns
6765

Function 7
----------
Matrix A: 
222 12 162 81 15 
183 198 214 102 215 
228 104 125 283 296 
152 86 19 35 58 
111 65 114 5 295 

Matrix B: 
58 213 11 5 39 
141 102 215 169 167 
169 84 269 229 175 
31 121 244 267 40 
229 234 199 62 93 

Matrix C = Matrix A * Matrix B: 
47892 75429 71349 62793 43647 
127095 139803 169822 123947 101728 
125570 173179 186449 141254 86983 
38520 60551 45355 32586 30409 
102579 109484 105787 57271 62769 

Function 8
----------
CS3060:CS3140:2
Ruby:HTML:4
IOLang:CSSadvance:5
Scala:JavaScript:5

Function 9
----------
Header: Item      Cost      Sold      Profit      
Row 1:  Keyboad   $10.00    $16.00    $6.00    
Row 2:  Monitor   $40.00    $80.00    $40.00    
Row 3:  MousePC   $10.00    $22.00    $12.00    
Row 4:  LenovoX   $90.00    $99.00    $9.00    
Row 5:  Macbook   $50.00    $99.00    $49.00    

Function 10
----------
I am a Shape
I am a Circle and a Shape
I am a Rectangle and a Shape
I am a Square, a Rectangle and a Shape

The radius of a Circle is 1.0
=> The area of this Circle is 3.1416
=> The perimeter of this Circle is 6.2832

The length of a Rectangle is 1.0, and the width of a Rectangle is 1.0
=> The area of this Rectangle is 1.0
=> The perimeter of this Rectangle is 4.0

The side of a Square is 2.0
=> The area of this Square is 4.0
=> The area of this Square is 8.0
[Finished in 2.7s]